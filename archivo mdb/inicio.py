#mostrar un mensaje por pantalla
print("hola mundo")

#declarar una variable
a=5
b=0
#operaciones aritmeticas
print(a*2)

#crear una función
def multiplica(a,b):
	return a*b

#llamada a una función
print(multiplica(3,4))
print(multiplica(3,a))

for i in range(5):
	print(i)
	print(multiplica(i,a))

for i in range(5):
	print(str(i)+'*'+str(a)+'='+str(multiplica(i,a)))

#tupla (inmutable  - entre paréntesis)
b=(1,2,4,5)
print("Estos es una tupla")
print(b);
print("primer elemento de la tupla")
print(b[1])

#listas (mutable)
c=[1,2,4,5]
c[2]=8
print(c)

#diccionario (valores asociados a claves)
d={'1':"texto",'cuatro':'texto2','lista':c}
print(d)

#input para que no se cierre la pantalla
input()