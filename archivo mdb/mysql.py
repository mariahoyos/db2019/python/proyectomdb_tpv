print("Hola Mundo")

import pymysql

############### CONFIGURAR ESTO ###################
# Abre conexion con la base de datos
conn = pymysql.connect("localhost","root","","provincias")
##################################################

# prepare a cursor object using cursor() method
cursor = conn.cursor()

cursor.execute("UPDATE provincias SET autonomia='Andalucía' WHERE autonomia IS  NULL;")

conn.commit()
# ejecuta el SQL query usando el metodo execute().
cursor.execute("SELECT VERSION()")
cursor.execute("select * from provincias")

data = cursor.fetchall()
print(data)

# procesa una unica linea usando el metodo fetchone().
#data = cursor.fetchone()
#print ("Database version : {0}".format(data))

# desconecta del servidor
conn.close()