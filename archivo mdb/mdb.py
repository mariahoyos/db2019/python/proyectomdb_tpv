#utilizar la biblioteca odbc para acceder a una base de datos en Access

import pyodbc

# set up some constants
MDB = 'emple_depart.mdb'
DRV = '{Microsoft Access Driver (*.mdb)}'
PWD = 'pw'

# connect to db
con = pyodbc.connect('DRIVER={};DBQ={};PWD={}'.format(DRV,MDB,PWD))
conn = con.cursor()
#obtener las tablas de una base de datos
for  t in conn.tables():
	print(t.table_name)

#obtener los campos de una tabla
for row in conn.columns(table='depart'):
    print (row.column_name)

# run a query and get the results 
SQL = 'SELECT * FROM emple;' # your query goes here
SQL = "UPDATE emple SET apellido='AROLLOs' WHERE apellido='ARRO%'"
rows = conn.execute(SQL)
conn.commit() #commit cierra la transacción

SQL = "SELECT * FROM emple WHERE apellido LIKE 'ARRO%';"
rows = conn.execute(SQL).fetchall()

print(rows)

conn.close()
con.close()
